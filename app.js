const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');

const trekbaseRoutes = require('./routes/trekbase');
const staRoutes = require('./routes/sta');
const errorController = require('./controllers/errorController');
const { port, databaseUrl } = require('./config/config');

const apiDocumentation = YAML.load('./openApiDocumentation.yaml');

const app = express();

// Middleware
app.use(cors());
app.use(bodyParser.json());

// API documentation
app.use(
  '/api-documentation',
  swaggerUi.serve,
  swaggerUi.setup(apiDocumentation),
);

// Trekbase routes
app.use('/trekbase', trekbaseRoutes);

// STA game routes
app.use('/sta', staRoutes);

// Error handling middleware
app.use(errorController.handleErrors);

// Connect to the DB and init the Application
mongoose
  .connect(databaseUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    app.listen(port);
  })
  .catch((err) => {
    const error = new Error('Could not connect to the database');
    error.statusCode = 500;
    error.payload = err;
    throw error;
  });

module.exports = app;
