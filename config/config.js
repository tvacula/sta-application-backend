const dotenv = require('dotenv');

dotenv.config();

const nodeEnv = process.env.NODE_ENV;
const port = process.env.PORT;
const mongoUser = process.env.MONGO_USERNAME;
const mongoPass = process.env.MONGO_PASSWORD;
const database = process.env.MONGO_DATABASE;
const databaseUrl = `mongodb+srv://${mongoUser}:${mongoPass}@cluster0-clwzi.mongodb.net/${database}?retryWrites=true&w=majority`;

module.exports = {
  nodeEnv,
  port,
  databaseUrl,
};
