const errorController = require('./errorController');
const Character = require('../models/sta/Character/Character');

exports.getCharacters = (req, res, next) => {
  Character
    .find()
    .populate('environment.selectedEnvironment')
    .populate('upbringing.selectedUpbringing')
    .populate('talents')
    .then((characters) => {
      if (!characters) {
        next(errorController.createError(
          'No characters found',
          404,
          [],
        ));
      }

      res.status(200).json({
        payload: characters,
      });
    })
    .catch((err) => {
      next(errorController.createError(
        'Error finding characters',
        500,
        err,
      ));
    });
};

exports.getCharacterById = async (req, res, next) => {
  const characterId = req.params.id;

  try {
    await Character.findOne(
      { _id: characterId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to find Character by ID: ${characterId}`,
            500,
            characterId,
          ));
          return;
        }

        doc
          .populate('environment.selectedEnvironment')
          .populate('upbringing.selectedUpbringing')
          .populate('talents')
          .execPopulate()
          .then((document) => {
            res.status(200).json({
              payload: document,
            });
          })
          .catch((error) => {
            if (error) {
              next(errorController.createError('Character creation failed', 500, error));
            }
          });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to find Character by ID: ${characterId}`,
      500,
      characterId,
    ));
  }
};

exports.createCharacter = async (req, res, next) => {
  const {
    name,
    age,
    gender,
    species,
    rank,
    environment,
    upbringing,
    attributes,
    disciplines,
    values,
    focuses,
    talents,
    stress,
    determination,
    serviceRecord,
    equipment,
    bio,
    milestones,
    reputation,
    commendations,
  } = req.body;

  const newCharacter = new Character({
    name,
    age,
    gender,
    species,
    rank,
    environment,
    upbringing,
    attributes,
    disciplines,
    values,
    focuses,
    talents,
    stress,
    determination,
    serviceRecord,
    equipment,
    bio,
    milestones,
    reputation,
    commendations,
  });

  try {
    await newCharacter.save((err, doc) => {
      if (err) {
        next(errorController.createError('Character creation failed', 500, err));
      }

      doc
        .populate('environment.selectedEnvironment')
        .populate('upbringing.selectedUpbringing')
        .populate('talents')
        .execPopulate()
        .then((document) => {
          res.status(201).json({
            payload: document,
          });
        })
        .catch((error) => {
          if (error) {
            next(errorController.createError('Character creation failed', 500, error));
          }
        });
    });
  } catch (err) {
    next(errorController.createError('Character creation failed', 500, err));
  }
};

exports.updateCharacter = async (req, res, next) => {
  const { charId } = req.params;

  const {
    name,
    age,
    gender,
    species,
    rank,
    environment,
    upbringing,
    attributes,
    disciplines,
    values,
    focuses,
    talents,
    stress,
    determination,
    serviceRecord,
    equipment,
    bio,
    milestones,
    reputation,
    commendations,
  } = req.body;

  try {
    await Character.findOneAndUpdate(
      { _id: charId },
      {
        name,
        age,
        gender,
        species,
        rank,
        environment,
        upbringing,
        attributes,
        disciplines,
        values,
        focuses,
        talents,
        stress,
        determination,
        serviceRecord,
        equipment,
        bio,
        milestones,
        reputation,
        commendations,
      },
      {
        new: true,
      },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            'Error updating character',
            500,
            charId,
          ));
        }

        doc
          .populate('environment.selectedEnvironment')
          .populate('upbringing.selectedUpbringing')
          .populate('talents')
          .execPopulate()
          .then((document) => {
            res.status(200).json({
              payload: document,
            });
          })
          .catch((error) => {
            if (error) {
              next(errorController.createError('Character creation failed', 500, error));
            }
          });
      },
    );
  } catch (err) {
    next(errorController.createError(
      'Update failed',
      500,
      err,
    ));
  }
};

exports.deleteCharacter = async (req, res, next) => {
  const { charId } = req.params;

  try {
    await Character.findByIdAndDelete(charId, (err, doc) => {
      if (err) {
        next(errorController.createError('Delete failed', 500, err));
      }

      res.status(200).json({
        payload: doc,
      });
    });
  } catch (err) {
    next(errorController.createError('Error deleting character', 500, err));
  }
};
