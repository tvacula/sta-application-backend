const errorController = require('./errorController');
const Environment = require('../models/sta/Environment/Environment');

exports.getEnvironments = async (req, res, next) => {
  try {
    const environments = await Environment.find();

    if (!environments) {
      next(errorController.createError(
        'No environments found',
        404,
        {},
      ));
      return;
    }

    res.status(200).json({
      payload: environments,
    });
  } catch (err) {
    next(errorController.createError(
      'Error finding environments',
      500,
      err,
    ));
  }
};

exports.getEnvironmentById = async (req, res, next) => {
  const { environmentId } = req.params;

  try {
    await Environment.findOne(
      { _id: environmentId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to find Environment by ID: ${environmentId}`,
            500,
            environmentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to find Environment by ID: ${environmentId}`,
      500,
      environmentId,
    ));
  }
};

exports.createEnvironment = async (req, res, next) => {
  const {
    name,
    description,
    attributes,
    disciplines,
  } = req.body;

  const newEnvironment = new Environment({
    name,
    description,
    attributes,
    disciplines,
  });

  await newEnvironment.save((err, doc) => {
    if (err) {
      next(errorController.createError(
        'Unable to create Environment',
        500,
        {},
      ));
      return;
    }

    res.status(201).json({
      payload: doc,
    });
  });
};

exports.updateEnvironment = async (req, res, next) => {
  const { environmentId } = req.params;
  const {
    name,
    description,
    attributes,
    disciplines,
  } = req.body;

  try {
    await Environment.findOneAndUpdate(
      { _id: environmentId },
      {
        name,
        description,
        attributes,
        disciplines,
      },
      {
        new: true,
      },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to update Environment ID: ${environmentId}`,
            500,
            environmentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to update Environment ID: ${environmentId}`,
      500,
      environmentId,
    ));
  }
};

exports.deleteEnvironmentById = async (req, res, next) => {
  const { environmentId } = req.params;

  try {
    await Environment.findOneAndDelete(
      { _id: environmentId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to delete Environment ID: ${environmentId}`,
            500,
            environmentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to delete Environment ID: ${environmentId}`,
      500,
      environmentId,
    ));
  }
};
