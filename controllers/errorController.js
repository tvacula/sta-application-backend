exports.handleErrors = (error, req, res, next) => {
  const status = error.statusCode || 500;
  const { message, payload } = error;

  res.status(status).json({
    message,
    payload,
  });
};

exports.createError = (message, statusCode, payload) => {
  const error = new Error(message);
  error.statusCode = statusCode;
  error.payload = payload;
  return error;
};
