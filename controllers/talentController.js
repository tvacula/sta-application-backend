const errorController = require('./errorController');
const Talent = require('../models/sta/Talent/Talent');

exports.getTalents = async (req, res, next) => {
  try {
    const talents = await Talent.find();

    if (!talents) {
      next(errorController.createError(
        'No talents found',
        404,
        {},
      ));
      return;
    }

    res.status(200).json({
      payload: talents,
    });
  } catch (err) {
    next(errorController.createError(
      'Error finding talents',
      500,
      err,
    ));
  }
};

exports.getTalentById = async (req, res, next) => {
  const { talentId } = req.params;

  try {
    await Talent.findOne(
      { _id: talentId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Error occurred finding Talent by ID: ${talentId}`,
            500,
            talentId,
          ));
          return;
        }

        if (!doc) {
          next(errorController.createError(
            `Unable to find Talent by ID: ${talentId}`,
            404,
            talentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Error occurred finding Talent by ID: ${talentId}`,
      500,
      talentId,
    ));
  }
};

exports.createTalent = async (req, res, next) => {
  const {
    name,
    description,
    category,
    requirements,
  } = req.body;

  const newTalent = new Talent({
    name,
    description,
    category,
    requirements,
  });

  await newTalent.save((err, doc) => {
    if (err) {
      next(errorController.createError(
        'Unable to create Talent',
        500,
        {},
      ));
      return;
    }

    res.status(201).json({
      payload: doc,
    });
  });
};

exports.updateTalent = async (req, res, next) => {
  const { talentId } = req.params;
  const {
    name,
    description,
    category,
    requirements,
  } = req.body;

  try {
    await Talent.findOneAndUpdate(
      { _id: talentId },
      {
        name,
        description,
        category,
        requirements,
      },
      {
        new: true,
      },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to update Talent ID: ${talentId}`,
            500,
            talentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to update Talent ID: ${talentId}`,
      500,
      talentId,
    ));
  }
};

exports.deleteTalentById = async (req, res, next) => {
  const { talentId } = req.params;

  try {
    await Talent.findOneAndDelete(
      { _id: talentId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to delete Talent ID: ${talentId}`,
            500,
            talentId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to delete Talent ID: ${talentId}`,
      500,
      talentId,
    ));
  }
};
