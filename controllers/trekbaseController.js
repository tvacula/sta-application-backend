const errorController = require('./errorController');
const Animal = require('../models/trekbase/Animal');

exports.getAnimals = async (req, res, next) => {
  try {
    const animals = await Animal.find();

    if (!animals) {
      next(errorController.createError('No Animals found', 404, []));
    }

    res.status(200).json({
      message: 'success!',
      payload: {
        count: animals.length,
        data: animals,
      },
    });
  } catch (err) {
    next(errorController.createError('Could not retrieve Animals', 500, err));
  }
};
