const errorController = require('./errorController');
const Upbringing = require('../models/sta/Upbringing/Upbringing');

exports.getUpbringings = async (req, res, next) => {
  try {
    const upbringings = await Upbringing.find();

    if (!upbringings) {
      next(errorController.createError(
        'No upbringings found',
        404,
        {},
      ));
      return;
    }

    res.status(200).json({
      payload: upbringings,
    });
  } catch (err) {
    next(errorController.createError(
      'Error finding upbringings',
      500,
      err,
    ));
  }
};

exports.getUpbringingById = async (req, res, next) => {
  const { upbringingId } = req.params;

  try {
    await Upbringing.findOne(
      { _id: upbringingId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to find Upbringing by ID: ${upbringingId}`,
            500,
            upbringingId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to find Upbringing by ID: ${upbringingId}`,
      500,
      upbringingId,
    ));
  }
};

exports.createUpbringing = async (req, res, next) => {
  const {
    name,
    description,
    accept,
    rebel,
    disciplines,
  } = req.body;

  const newUpbringing = new Upbringing({
    name,
    description,
    accept,
    rebel,
    disciplines,
  });

  await newUpbringing.save((err, doc) => {
    if (err) {
      next(errorController.createError(
        'Unable to create Upbringing',
        500,
        {},
      ));
      return;
    }

    res.status(201).json({
      payload: doc,
    });
  });
};

exports.updateUpbringing = async (req, res, next) => {
  const { upbringingId } = req.params;
  const {
    name,
    description,
    accept,
    rebel,
    disciplines,
  } = req.body;

  try {
    await Upbringing.findOneAndUpdate(
      { _id: upbringingId },
      {
        name,
        description,
        accept,
        rebel,
        disciplines,
      },
      {
        new: true,
      },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to update Upbringing ID: ${upbringingId}`,
            500,
            upbringingId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to update Upbringing ID: ${upbringingId}`,
      500,
      upbringingId,
    ));
  }
};

exports.deleteUpbringingById = async (req, res, next) => {
  const { upbringingId } = req.params;

  try {
    await Upbringing.findOneAndDelete(
      { _id: upbringingId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to delete Upbringing ID: ${upbringingId}`,
            500,
            upbringingId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to delete Upbringing ID: ${upbringingId}`,
      500,
      upbringingId,
    ));
  }
};
