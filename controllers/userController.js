const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');

const errorController = require('./errorController');
const User = require('../models/sta/User');

exports.getUserById = async (req, res, next) => {
  const userId = req.params.id;

  try {
    const foundUser = await User.findById(userId);

    if (!foundUser) {
      next(errorController.createError('User not found', 500, userId));
    }

    res.status(200).json({
      message: 'User found',
      payload: foundUser,
    });
  } catch (err) {
    next(errorController.createError('Could not retrieve User', 500, err));
  }
};

exports.createUser = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    next(errorController.createError('Validation failed', 422, errors.array()));
  }

  const {
    userName, email, password, imagePath,
  } = req.body;

  const hashedPassword = await bcrypt.hash(password, 12);

  const newUser = new User({
    userName,
    email,
    password: hashedPassword,
    imagePath,
    characters: [],
  });

  try {
    newUser.save((err) => {
      if (err) {
        next(errorController.createError('User creation failed', 500, err));
      }

      res.status(201).json({
        message: 'User created',
        payload: newUser,
      });
    });
  } catch (err) {
    next(errorController.createError('User creation failed', 500, err));
  }
};

exports.updateUser = async (req, res, next) => {
  const userId = req.params.id;
  const {
    userName, email, password, imagePath,
  } = req.body;

  try {
    const userToEdit = await User.findById(userId);

    userToEdit.userName = userName;
    userToEdit.email = email;
    userToEdit.password = password;
    userToEdit.imagePath = imagePath;

    userToEdit.save((err) => {
      if (err) {
        next(errorController.createError('User update failed', 500, err));
      }

      res.status(200).json({
        message: 'User updated',
        payload: userToEdit,
      });
    });
  } catch (err) {
    next(errorController.createError('User update failed', 500, err));
  }
};

exports.deleteUser = async (req, res, next) => {
  const userId = req.params.id;

  try {
    const userToDelete = await User.findById(userId);

    if (!userToDelete) {
      next(errorController.createError('User not found', 404, userId));
    }

    userToDelete.remove();

    res.status(200).json({
      message: 'User deleted',
      payload: {
        _id: userId,
      },
    });
  } catch (err) {
    next(errorController.createError('User deletion failed', 500, err));
  }
};
