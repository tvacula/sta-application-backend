const errorController = require('./errorController');
const Weapon = require('../models/sta/Weapon/Weapon');

exports.getWeapons = async (req, res, next) => {
  try {
    const weapons = await Weapon.find();

    if (!weapons) {
      next(errorController.createError(
        'No weapons found',
        404,
        {},
      ));
      return;
    }

    res.status(200).json({
      payload: weapons,
    });
  } catch (err) {
    next(errorController.createError(
      'Error finding weapons',
      500,
      err,
    ));
  }
};

exports.getWeaponById = async (req, res, next) => {
  const { weaponId } = req.params;

  try {
    await Weapon.findOne(
      { _id: weaponId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to find Weapon by ID: ${weaponId}`,
            500,
            weaponId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to find Weapon by ID: ${weaponId}`,
      500,
      weaponId,
    ));
  }
};

exports.createWeapon = async (req, res, next) => {
  const {
    name,
    description,
    type,
    size,
    damageType,
    qualities,
    cost,
  } = req.body;

  const newWeapon = new Weapon({
    name,
    description,
    type,
    size,
    damageType,
    qualities,
    cost,
  });

  await newWeapon.save((err, doc) => {
    if (err) {
      next(errorController.createError(
        'Unable to create Weapon',
        500,
        {},
      ));
      return;
    }

    res.status(201).json({
      payload: doc,
    });
  });
};

exports.updateWeapon = async (req, res, next) => {
  const { weaponId } = req.params;
  const {
    name,
    description,
    type,
    size,
    damageType,
    qualities,
    cost,
  } = req.body;

  try {
    await Weapon.findOneAndUpdate(
      { _id: weaponId },
      {
        name,
        description,
        type,
        size,
        damageType,
        qualities,
        cost,
      },
      {
        new: true,
      },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to update Weapon ID: ${weaponId}`,
            500,
            weaponId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to update Weapon ID: ${weaponId}`,
      500,
      weaponId,
    ));
  }
};

exports.deleteWeaponById = async (req, res, next) => {
  const { weaponId } = req.params;

  try {
    await Weapon.findOneAndDelete(
      { _id: weaponId },
      (err, doc) => {
        if (err) {
          next(errorController.createError(
            `Unable to delete Weapon ID: ${weaponId}`,
            500,
            weaponId,
          ));
          return;
        }

        res.status(200).json({
          payload: doc,
        });
      },
    );
  } catch (err) {
    next(errorController.createError(
      `Unable to delete Weapon ID: ${weaponId}`,
      500,
      weaponId,
    ));
  }
};
