const mongoose = require('mongoose');

const { Schema } = mongoose;

const { genderEnumList, speciesEnumList } = require('../Common/Enums');

const {
  attributeSchema,
  environmentSchema,
  milestonesSchema,
  upbringingSchema,
} = require('./characterSchemas');

const {
  dispciplineSchema,
  currentAndMaxSchema,
} = require('../Common/subSchemas');

const characterSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  gender: {
    type: String,
    enum: genderEnumList,
    required: true,
  },
  species: {
    type: String,
    enum: speciesEnumList,
    required: true,
  },
  rank: {
    type: String,
    required: true,
  },
  environment: {
    type: environmentSchema,
    required: true,
  },
  upbringing: {
    type: upbringingSchema,
    required: true,
  },
  attributes: {
    type: attributeSchema,
    required: true,
  },
  disciplines: {
    type: dispciplineSchema,
    required: true,
  },
  values: [
    {
      type: String,
      required: true,
    },
  ],
  focuses: [
    {
      type: String,
      required: true,
    },
  ],
  talents: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Talent',
      required: false,
    },
  ],
  stress: {
    type: currentAndMaxSchema,
    required: true,
  },
  determination: {
    type: currentAndMaxSchema,
    required: true,
  },
  serviceRecord: [
    {
      type: Schema.Types.ObjectId,
      ref: 'ServiceRecord',
      required: false,
    },
  ],
  equipment: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Item',
      required: false,
    },
  ],
  bio: {
    type: String,
    required: true,
  },
  milestones: {
    type: milestonesSchema,
    required: true,
  },
  reputation: {
    type: Number,
    required: true,
  },
  commendations: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Commendation',
      required: false,
    },
  ],
});

module.exports = mongoose.model('Character', characterSchema);
