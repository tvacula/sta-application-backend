const mongoose = require('mongoose');

const { Schema } = mongoose;

const { upbringingStatusEnum } = require('../Common/Enums');
const { statModifierSchema } = require('../Common/subSchemas');

const milestonesSchema = new Schema(
  {
    regular: {
      type: Number,
      required: true,
    },
    spotlight: {
      type: Number,
      required: true,
    },
    arc: {
      type: Number,
      required: true,
    },
  },
  { _id: false },
);

const environmentSchema = new Schema(
  {
    selectedEnvironment: {
      type: Schema.Types.ObjectId,
      ref: 'Environment',
      required: false,
    },
    selectedStats: [
      {
        type: statModifierSchema,
        required: false,
      },
    ],
  },
  { _id: false },
);

const upbringingSchema = new Schema(
  {
    selectedUpbringing: {
      type: Schema.Types.ObjectId,
      ref: 'Upbringing',
      required: false,
    },
    status: {
      type: String,
      enum: upbringingStatusEnum,
      required: false,
    },
    selectedStats: [
      {
        type: statModifierSchema,
        required: false,
      },
    ],
  },
  { _id: false },
);

const attributeSchema = new Schema(
  {
    control: {
      type: Number,
      required: true,
    },
    daring: {
      type: Number,
      required: true,
    },
    fitness: {
      type: Number,
      required: true,
    },
    insight: {
      type: Number,
      required: true,
    },
    presence: {
      type: Number,
      required: true,
    },
    reason: {
      type: Number,
      required: true,
    },
  },
  { _id: false },
);

module.exports = {
  attributeSchema,
  environmentSchema,
  milestonesSchema,
  upbringingSchema,
};
