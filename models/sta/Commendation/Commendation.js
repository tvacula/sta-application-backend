const mongoose = require('mongoose');

const { Schema } = mongoose;

const commendationSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  cost: {
    type: Number,
    required: true,
  },
  conditions: {
    type: String,
    required: true,
  },
  benefit: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Commendation', commendationSchema);
