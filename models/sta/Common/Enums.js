exports.attributeEnumList = [
  'control',
  'daring',
  'fitness',
  'insight',
  'presence',
  'reason',
];

exports.disciplineEnumList = [
  'command',
  'conn',
  'security',
  'engineering',
  'science',
  'medicine',
];

exports.genderEnumList = ['male', 'female', 'other'];

exports.speciesEnumList = [
  'andorian',
  'ardanan',
  'bajoran',
  'betazoid',
  'bolian',
  'benzite',
  'denobulan',
  'deltan',
  'efrosian',
  'ferengi',
  'human',
  'trill',
  'klingon',
  'q',
  'rigellian chelon',
  'rigellian jelna',
  'risian',
  'tellarite',
  'vulcan',
  'xindi-arboreal',
  'xindi-primate',
  'xindi-reptilian',
  'xindi-insectoid',
  'zakdorn',
];

exports.upbringingStatusEnum = ['accept', 'rebel'];

exports.requirementTypeEnumList = ['race', 'discipline', 'attribute'];

exports.categoryEnumList = [
  'general',
  'racial',
  'command',
  'conn',
  'security',
  'engineering',
  'science',
  'medicine',
  'ship',
];

exports.statTypeEnum = ['attribute', 'discipline'];

exports.weaponTypeEnum = ['melee', 'ranged'];

exports.weaponSizeEnum = ['one-handed', 'two-handed'];

exports.shipRoleEnum = [
  'captain',
  'first-officer',
  'second-officer',
  'chief security officer',
  'chief medical officer',
  'chief science officer',
  'chief engineering officer',
  'conn',
  'navigator',
  'medical officer',
  'security officer',
  'engineering officer',
  'tactical officer',
  'communications officer',
  "ship's counselor",
  'other',
];

exports.crewMemberTypeEnum = ['player', 'supporting', 'npc'];

exports.serviceLocationTypeEnum = ['station', 'planet', 'starship'];
