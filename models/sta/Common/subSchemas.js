const mongoose = require('mongoose');

const { Schema } = mongoose;

const { statTypeEnum } = require('../Common/Enums');

const currentAndMaxSchema = new Schema(
  {
    current: {
      type: Number,
      required: true,
    },
    maximum: {
      type: Number,
      required: true,
    },
  },
  { _id: false },
);

const statModifierSchema = new Schema(
  {
    stat: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      enum: statTypeEnum,
      required: true,
    },
    modifier: {
      type: Number,
      required: true,
    },
  },
  { _id: false },
);

const dispciplineSchema = new Schema(
  {
    command: {
      type: Number,
      required: true,
    },
    conn: {
      type: Number,
      required: true,
    },
    security: {
      type: Number,
      required: true,
    },
    engineering: {
      type: Number,
      required: true,
    },
    science: {
      type: Number,
      required: true,
    },
    medicine: {
      type: Number,
      required: true,
    },
  },
  { _id: false },
);

module.exports = {
  currentAndMaxSchema,
  statModifierSchema,
  dispciplineSchema,
};
