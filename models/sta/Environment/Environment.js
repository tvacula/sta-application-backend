const mongoose = require('mongoose');

const { Schema } = mongoose;

const {
  statModifierSchema,
} = require('../Common/subSchemas');

const environmentSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  attributes: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
  disciplines: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Environment', environmentSchema);
