const mongoose = require('mongoose');

const { Schema } = mongoose;

const itemSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  effects: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Effect',
      required: false,
    },
  ],
});

module.exports = mongoose.model('Item', itemSchema);
