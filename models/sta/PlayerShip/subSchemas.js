const mongoose = require('mongoose');

const { Schema } = mongoose;
const { shipRoleEnum, crewMemberTypeEnum } = require('../Common/Enums');

const crewMemberSchema = new Schema({
  character: {
    type: Schema.Types.ObjectId,
    required: false,
  },
  role: {
    type: String,
    enum: shipRoleEnum,
    required: true,
  },
  type: {
    type: String,
    enum: crewMemberTypeEnum,
    required: true,
  },
});

module.exports = {
  crewMemberSchema,
};
