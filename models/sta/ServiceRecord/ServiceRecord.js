const mongoose = require('mongoose');

const { Schema } = mongoose;
const { serviceLocationTypeEnum } = require('../Common/Enums');
const { colleagueSchema } = require('./subSchemas');

const serviceRecordSchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  evaluation: {
    type: String,
    required: true,
  },
  character: {
    type: Schema.Types.ObjectId,
    ref: 'Character',
    required: true,
  },
  startDate: {
    type: Number,
    required: true,
  },
  endDate: {
    type: Number,
    required: false,
  },
  locationType: {
    type: String,
    enum: serviceLocationTypeEnum,
    required: true,
  },
  servedWith: [
    {
      type: colleagueSchema,
      required: false,
    },
  ],
});

module.exports = mongoose.model('ServiceRecord', serviceRecordSchema);
