const mongoose = require('mongoose');

const { Schema } = mongoose;

const colleagueSchema = new Schema({
  character: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  relationshipType: {
    type: String,
    required: true,
  },
});

module.exports = {
  colleagueSchema,
};
