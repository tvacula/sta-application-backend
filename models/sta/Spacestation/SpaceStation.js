const mongoose = require('mongoose');

const { Schema } = mongoose;

const { shipSystemsSchema } = require('../Starship/subSchemas');
const {
  dispciplineSchema,
  currentAndMaxSchema,
} = require('../Common/subSchemas');

const { crewMemberSchema } = require('../PlayerShip/subSchemas');

const spaceStationSchema = new Schema({
  name: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  systems: {
    type: shipSystemsSchema,
    required: true,
  },
  departments: {
    type: dispciplineSchema,
    required: true,
  },
  talents: [
    {
      type: Schema.Types.ObjectId,
      ref: 'ShipTalent',
      required: false,
    },
  ],
  crew: [
    {
      type: crewMemberSchema,
      required: false,
    },
  ],
  power: {
    type: currentAndMaxSchema,
    required: true,
  },
  shields: {
    type: currentAndMaxSchema,
    required: true,
  },
  resistance: {
    type: Number,
    required: true,
  },
  crewSupport: {
    type: Number,
    required: true,
  },
  traits: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Trait',
      required: false,
    },
  ],
});

module.exports = mongoose.model('SpaceStation', spaceStationSchema);
