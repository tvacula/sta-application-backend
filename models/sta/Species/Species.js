const mongoose = require('mongoose');

const { Schema } = mongoose;

const { statModifierSchema } = require('../Common/subSchemas');

const speciesSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  attributes: [
    {
      type: statModifierSchema,
      required: false,
    },
  ],
  traits: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Trait',
      required: false,
    },
  ],
  talents: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Talent',
      required: false,
    },
  ],
});

module.exports = mongoose.model('Species', speciesSchema);
