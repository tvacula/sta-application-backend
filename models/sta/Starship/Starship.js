const mongoose = require('mongoose');

const { Schema } = mongoose;

const { shipSystemsSchema } = require('./subSchemas');
const { dispciplineSchema } = require('../Common/subSchemas');

const starshipSchema = new Schema({
  name: {
    type: String,
    required: false,
  },
  inServiceFrom: {
    type: Number,
    required: true,
  },
  overview: {
    type: String,
    required: true,
  },
  capabilities: {
    type: String,
    required: true,
  },
  systems: {
    type: shipSystemsSchema,
    required: true,
  },
  departments: {
    type: dispciplineSchema,
    required: true,
  },
  scale: {
    type: Number,
    required: true,
  },
  attacks: {
    type: Array,
    required: false,
  },
  talents: [
    {
      type: Schema.Types.ObjectId,
      ref: 'ShipTalent',
      required: false,
    },
  ],
});

module.exports = mongoose.model('Starship', starshipSchema);
