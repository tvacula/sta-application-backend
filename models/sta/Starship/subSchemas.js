const mongoose = require('mongoose');

const { Schema } = mongoose;

const shipSystemsSchema = new Schema({
  engines: {
    type: Number,
    required: true,
  },
  computers: {
    type: Number,
    required: true,
  },
  weapons: {
    type: Number,
    required: true,
  },
  structure: {
    type: Number,
    required: true,
  },
  sensors: {
    type: Number,
    required: true,
  },
  communications: {
    type: Number,
    required: true,
  },
});

module.exports = {
  shipSystemsSchema,
};
