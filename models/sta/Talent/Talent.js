const mongoose = require('mongoose');

const { Schema } = mongoose;

const { categoryEnumList } = require('../Common/Enums');
const { statModifierSchema } = require('../Common/subSchemas');

const talentSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    enum: categoryEnumList,
    required: true,
  },
  requirements: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Talent', talentSchema);
