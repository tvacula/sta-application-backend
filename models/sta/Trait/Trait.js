const mongoose = require('mongoose');

const { Schema } = mongoose;

const traitSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Trait', traitSchema);
