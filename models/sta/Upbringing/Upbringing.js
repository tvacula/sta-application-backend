const mongoose = require('mongoose');

const { Schema } = mongoose;

const { statModifierSchema } = require('../Common/subSchemas');

const upbringingSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  accept: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
  rebel: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
  disciplines: [
    {
      type: statModifierSchema,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Upbringing', upbringingSchema);
