const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  userName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  imagePath: {
    type: String,
    required: false,
  },
  characters: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Character',
    },
  ],
});

module.exports = mongoose.model('User', userSchema);
