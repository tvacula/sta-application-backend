const mongoose = require('mongoose');

const { Schema } = mongoose;

const { damageTypeSchema } = require('./subSchemas');

const {
  weaponTypeEnum,
  weaponSizeEnum,
} = require('../Common/Enums');

const weaponSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: weaponTypeEnum,
    required: true,
  },
  size: {
    type: String,
    enum: weaponSizeEnum,
    required: true,
  },
  damageType: {
    type: damageTypeSchema,
    required: true,
  },
  qualities: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Effect',
      required: false,
    },
  ],
  cost: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Effect',
      required: false,
    },
  ],
});

module.exports = mongoose.model('Weapon', weaponSchema);
