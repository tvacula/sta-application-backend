const mongoose = require('mongoose');

const { Schema } = mongoose;

const damageTypeSchema = new Schema({
  numberOfDice: {
    type: Number,
    required: true,
  },
  effects: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Effect',
      required: false,
    },
  ],
},
{
  _id: false,
});

module.exports = {
  damageTypeSchema,
};
