const mongoose = require('mongoose');

const { Schema } = mongoose;

const animalTypeEnumList = [
  'avian',
  'canine',
  'feline',
  'fish',
  'insect',
  'unknown',
];

const animalSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: animalTypeEnumList,
    default: 'unknown',
  },
  planetOfOrigin: {
    type: String,
    default: 'unknown',
  },
});

module.exports = mongoose.model('Animal', animalSchema);
