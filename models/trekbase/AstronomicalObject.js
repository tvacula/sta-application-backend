const mongoose = require('mongoose');

const { Schema } = mongoose;

const astronomicalObjectTypeEnumList = [
  'PLANET',
  'D_CLASS_PLANET',
  'H_CLASS_PLANET',
  'GAS_GIANT_PLANET',
  'K_CLASS_PLANET',
  'L_CLASS_PLANET',
  'M_CLASS_PLANET',
  'Y_CLASS_PLANET',
  'ROGUE_PLANET',
  'ARTIFICIAL_PLANET',
  'ASTEROID',
  'ASTEROIDAL_MOON',
  'ASTEROID_BELT',
  'CLUSTER',
  'COMET',
  'CONSTELLATION',
  'GALAXY',
  'MOON',
  'M_CLASS_MOON',
  'NEBULA',
  'PLANETOID',
  'D_CLASS_PLANETOID',
  'QUASAR',
  'STAR',
  'STAR_SYSTEM',
  'SECTOR',
  'REGION',
  'UNKNOWN',
];

const astronomicalObjectSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: astronomicalObjectTypeEnumList,
    required: true,
  },
  location: {
    type: String,
    default: 'unknown',
  },
});

module.exports = mongoose.model('AstronomicalObject', astronomicalObjectSchema);
