const mongoose = require('mongoose');

const { Schema } = mongoose;

const genderEnumList = [
  'male',
  'female',
  'hermaphrodite',
  'none',
  'other',
  'unknown',
];

const typeEnumList = ['android', 'hologram', 'biological', 'other', 'unknown'];

const dateAndLocationSchema = new Schema(
  {
    date: {
      type: Number,
      required: true,
    },
    location: {
      type: String,
      required: true,
    },
  },
  { _id: false },
);

const characterSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    enum: genderEnumList,
    default: 'unknown',
  },
  birthData: {
    type: dateAndLocationSchema,
    required: true,
  },
  deathData: {
    type: dateAndLocationSchema,
    required: true,
  },
  species: {
    type: Schema.Types.ObjectId,
    ref: 'TrekbaseSpecies',
    required: true,
  },
  occupation: {
    type: Schema.Types.ObjectId,
    ref: 'Occupation',
    required: true,
  },
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization',
    required: true,
  },
  serialNumber: {
    type: String,
  },
  type: {
    type: String,
    enum: typeEnumList,
    default: 'unknown',
  },
  fictional: {
    type: Boolean,
    required: true,
  },
  mirror: {
    type: Boolean,
    required: true,
  },
});

module.exports = mongoose.model('TrekbaseCharacter', characterSchema);
