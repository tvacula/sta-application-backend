const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'earthConflict',
  'federationWar',
  'klingonWar',
  'dominionWar',
  'other',
];

const datesType = new Schema(
  {
    from: {
      type: String,
      required: true,
    },
    to: {
      type: String,
      required: true,
    },
  },
  { _id: false },
);

const conflictSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  dates: {
    type: datesType,
    required: true,
  },
  locations: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
  ],
  type: {
    type: String,
    enum: typeEnumList,
    required: true,
  },
  alternateReality: {
    type: Boolean,
    required: true,
  },
});

module.exports = mongoose.model('Conflict', conflictSchema);
