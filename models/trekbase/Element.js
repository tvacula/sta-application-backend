const mongoose = require('mongoose');

const { Schema } = mongoose;

const elementSchema = new Schema({
  name: {
    type: String,
    require: true,
  },
  symbol: {
    type: String,
    default: 'unknown',
  },
});

module.exports = mongoose.model('Element', elementSchema);
