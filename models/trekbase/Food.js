const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'dessert',
  'fruit',
  'herbOrSpice',
  'sauce',
  'soup',
  'beverage',
  'alcohol',
  'juice',
  'tea',
];

const foodSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  origin: {
    type: String,
    default: 'unknown',
  },
  type: {
    type: String,
    enum: typeEnumList,
    required: true,
  },
});

module.exports = mongoose.model('Food', foodSchema);
