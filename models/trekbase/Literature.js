const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'shakespearean',
  'report',
  'scientific',
  'technical',
  'religious',
  'unknown',
  'other',
];

const literatureSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: typeEnumList,
    required: true,
  },
});

module.exports = mongoose.model('Literature', literatureSchema);
