const mongoose = require('mongoose');

const { Schema } = mongoose;

const realityEnumList = ['mirror', 'alternate', 'normal'];

const typeEnumList = [
  'earthly',
  'bajoran',
  'fictional',
  'religious',
  'geographical',
  'bodyOfWater',
  'country',
  'subnationalEntity',
  'settlement',
  'colony',
  'landform',
  'landmark',
  'road',
  'structure',
  'shipyard',
  'buildingInterior',
  'establishment',
  'medicalEstablishment',
  'ds9Establishment',
  'school',
  'unknown',
];

const locationSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  reality: {
    type: String,
    enum: realityEnumList,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Location', locationSchema);
