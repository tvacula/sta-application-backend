const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'chemical',
  'biochemical',
  'drug',
  'poisonous',
  'explosive',
  'gemstone',
  'alloyOrComposite',
  'fuel',
  'mineral',
  'preciousMaterial',
];

const materialSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Material', materialSchema);
