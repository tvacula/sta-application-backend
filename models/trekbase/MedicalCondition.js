const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = ['physical', 'psychological'];

const medicalConditionSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: typeEnumList,
    default: 'physical',
  },
});

module.exports = mongoose.model('MedicalCondition', medicalConditionSchema);
