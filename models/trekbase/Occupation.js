const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = ['legal', 'medical', 'scientific', 'other'];

const occupationSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: typeEnumList,
    default: 'other',
  },
});

module.exports = mongoose.model('Occupation', occupationSchema);
