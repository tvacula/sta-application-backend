const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'governmental',
  'intergovernmental',
  'research',
  'sport',
  'medical',
  'military',
  'militaryUnit',
  'governmentAgency',
  'lawEnforcement',
  'prisonOrPenalColony',
  'other',
];

const realityEnumList = ['mirror', 'alternate', 'normal'];

const organizationSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      default: 'other',
    },
  ],
  reality: {
    type: String,
    enum: realityEnumList,
    default: 'normal',
  },
});

module.exports = mongoose.model('Organization', organizationSchema);
