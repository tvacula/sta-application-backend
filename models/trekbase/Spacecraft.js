const mongoose = require('mongoose');

const { Schema } = mongoose;

const spacecraftSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  registry: {
    type: String,
    default: 'unknown',
  },
  class: {
    type: Schema.Types.ObjectId,
    ref: 'SpacecraftClass',
    required: true,
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'Organization',
    required: true,
  },
});

module.exports = mongoose.model('Spacecraft', spacecraftSchema);
