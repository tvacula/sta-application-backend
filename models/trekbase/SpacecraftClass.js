const mongoose = require('mongoose');

const { Schema } = mongoose;

const realityEnumList = ['alternate', 'mirror', 'normal'];

const classData = new Schema(
  {
    numberOfDecks: {
      type: Number,
      required: true,
    },
    warpCapable: {
      type: Boolean,
      required: true,
    },
  },
  { _id: false },
);

const serviceData = new Schema(
  {
    from: {
      type: String,
      default: 'unknown',
    },
    to: {
      type: String,
      default: 'unknown',
    },
  },
  { _id: false },
);

const spacecraftClassSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  classData: {
    type: classData,
    required: true,
  },
  reality: {
    type: String,
    enum: realityEnumList,
    default: 'normal',
  },
  service: {
    type: serviceData,
    required: true,
  },
  species: {
    type: Schema.Types.ObjectId,
    ref: 'TrekbaseSpecies',
    required: true,
  },
});

module.exports = mongoose.model('SpacecraftClass', spacecraftClassSchema);
