const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'extinct',
  'warpCapable',
  'extraGalactic',
  'humanoid',
  'reptillian',
  'nonCorporeal',
  'shapeshifting',
  'spaceborne',
  'telepathic',
  'transdimensional',
  'unnamed',
];

const realityEnumList = ['mirror', 'alternate', 'normal'];

const speciesSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  homeworld: {
    type: Schema.Types.ObjectId,
    ref: 'AstronomicalObject',
  },
  quadrant: {
    type: Schema.Types.ObjectId,
    ref: 'AstronomicalObject',
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
  reality: {
    type: String,
    enum: realityEnumList,
    required: true,
  },
});

module.exports = mongoose.model('TrekbaseSpecies', speciesSchema);
