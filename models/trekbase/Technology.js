const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = [
  'borgComponent',
  'borg',
  'communications',
  'computer',
  'computerProgramming',
  'subroutine',
  'database',
  'energy',
  'fictional',
  'holographic',
  'identification',
  'lifeSupport',
  'sensor',
  'shield',
  'medical',
  'transporter',
  'tool',
  'culinaryTool',
  'engineeringTool',
  'householdTool',
];

const technologySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
});

module.exports = mongoose.model('Technology', technologySchema);
