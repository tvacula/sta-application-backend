const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = ['military', 'fleet', 'religious', 'position', 'mirror'];
const realityEnumList = ['mirror', 'alternate', 'normal'];

const titleSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
  reality: {
    type: String,
    enum: realityEnumList,
    required: true,
  },
});

module.exports = mongoose.model('Title', titleSchema);
