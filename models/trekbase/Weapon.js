const mongoose = require('mongoose');

const { Schema } = mongoose;

const typeEnumList = ['handheld', 'laser', 'plasma', 'photonic', 'phaser'];

const realityEnumList = ['mirror', 'alternate', 'normal'];

const weaponSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: [
    {
      type: String,
      enum: typeEnumList,
      required: true,
    },
  ],
  reality: {
    type: String,
    enum: realityEnumList,
    required: true,
  },
});

module.exports = mongoose.model('Weapon', weaponSchema);
