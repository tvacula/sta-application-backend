const mongoose = require('mongoose');

const { Schema } = mongoose;

const errorLogSchema = new Schema(
  {
    errorCode: {
      type: Number,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('ErrorLog', errorLogSchema);
