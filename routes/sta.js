const express = require('express');

const Router = express.Router({
  strict: true,
});

const userRouter = require('../routes/sta/userRouter');
const characterRouter = require('../routes/sta/characterRouter');
const talentRouter = require('../routes/sta/talentRouter');
const environmentRouter = require('../routes/sta/environmentRouter');
const upbringingRouter = require('../routes/sta/upbringingRouter');
const weaponRouter = require('../routes/sta/weaponRouter');

/* USER ROUTES */
Router.use('/user', userRouter);

/* CHARACTER ROUTES */
Router.use('/character', characterRouter);

/* TALENT ROUTES */
Router.use('/talent', talentRouter);

/* ENVIRONMENT ROUTES */
Router.use('/environment', environmentRouter);

/* UPBRINGING ROUTES */
Router.use('/upbringing', upbringingRouter);

/* WEAPON ROUTES */
Router.use('/weapon', weaponRouter);

module.exports = Router;
