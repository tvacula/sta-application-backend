const express = require('express');

const characterController = require('../../controllers/characterController');
const characterValidator = require('../../validators/characterValidator');

const Router = express.Router({
  strict: true,
});

// Get all characters
Router.get('/', characterController.getCharacters);

// Create a character
Router.post(
  '/create',
  characterValidator.validateCreateOrUpdateRequest,
  characterController.createCharacter,
);

// Get character by ID
Router.get('/:id', characterController.getCharacterById);

// Update a character
Router.put(
  '/:charId/edit',
  characterValidator.validateCreateOrUpdateRequest,
  characterController.updateCharacter,
);

// Delete a character
Router.delete('/:charId/delete', characterController.deleteCharacter);

module.exports = Router;
