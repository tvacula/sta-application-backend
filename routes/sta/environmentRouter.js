const express = require('express');

const environmentController = require('../../controllers/environmentController');
const environmentValidator = require('../../validators/environmentValidator');

const Router = express.Router({
  strict: true,
});

// Get all environments
Router.get('/', environmentController.getEnvironments);

// Create an environment
Router.post(
  '/create',
  environmentValidator.validateCreateOrUpdateRequest,
  environmentController.createEnvironment,
);

// Get environment by ID
Router.get('/:environmentId', environmentController.getEnvironmentById);

// Update an environment
Router.put(
  '/:environmentId/edit',
  environmentValidator.validateCreateOrUpdateRequest,
  environmentController.updateEnvironment,
);

// Delete an environment
Router.delete(
  '/:environmentId/delete',
  environmentController.deleteEnvironmentById,
);

module.exports = Router;
