const express = require('express');

const Router = express.Router({
  strict: true,
});

const talentController = require('../../controllers/talentController');
const talentValidator = require('../../validators/talentValidator');

// Get all talents
Router.get('/', talentController.getTalents);

// Create a talent
Router.post(
  '/create',
  talentValidator.validateCreateOrUpdateRequest,
  talentController.createTalent,
);

// Get talent by ID
Router.get('/:talentId', talentController.getTalentById);

// Update a talent
Router.put(
  '/:talentId/edit',
  talentValidator.validateCreateOrUpdateRequest,
  talentController.updateTalent,
);

// Delete a talent
Router.delete('/:talentId/delete', talentController.deleteTalentById);

module.exports = Router;
