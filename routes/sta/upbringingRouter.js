const express = require('express');

const upbringingController = require('../../controllers/upbringingController');
const upbringingValidator = require('../../validators/upbringingValidator');

const Router = express.Router({
  strict: true,
});

// Get all upbringings
Router.get('/', upbringingController.getUpbringings);

// Create an upbringing
Router.post(
  '/create',
  upbringingValidator.validateCreateOrUpdateRequest,
  upbringingController.createUpbringing,
);

// Get upbringing by ID
Router.get('/:upbringingId', upbringingController.getUpbringingById);

// Update an upbringing
Router.put(
  '/:upbringingId/edit',
  upbringingValidator.validateCreateOrUpdateRequest,
  upbringingController.updateUpbringing,
);

// Delete an upbringing
Router.delete(
  '/:upbringingId/delete',
  upbringingController.deleteUpbringingById,
);

module.exports = Router;
