const express = require('express');

const userController = require('../../controllers/userController');

const Router = express.Router({
  strict: true,
});

// Get User by ID {method: GET}
Router.get('/:id', userController.getUserById);

// Create User {method: POST}
Router.post('/create', userController.createUser);

// Update User {method: PUT}
Router.put('/:id/edit', userController.updateUser);

// Delete User {method: DELETE}
Router.delete('/:id/delete', userController.deleteUser);

module.exports = Router;
