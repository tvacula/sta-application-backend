const express = require('express');

const weaponController = require('../../controllers/weaponController');
const weaponValidator = require('../../validators/weaponValidator');

const Router = express.Router({
  strict: true,
});

// Get all weapons
Router.get('/', weaponController.getWeapons);

// Create an weapon
Router.post(
  '/create',
  weaponValidator.validateCreateOrUpdateRequest,
  weaponController.createWeapon,
);

// Get weapon by ID
Router.get('/:weaponId', weaponController.getWeaponById);

// Update an upbringing
Router.put(
  '/:weaponId/edit',
  weaponValidator.validateCreateOrUpdateRequest,
  weaponController.updateWeapon,
);

// Delete an weapon
Router.delete('/:weaponId/delete', weaponController.deleteWeaponById);

module.exports = Router;
