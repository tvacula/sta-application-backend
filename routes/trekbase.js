const express = require('express');

const Router = express.Router();

const trekbaseController = require('../controllers/trekbaseController');

Router.get('/animal', trekbaseController.getAnimals);

module.exports = Router;
