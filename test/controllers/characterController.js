const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const Character = require('../../models/sta/Character/Character');
const Talent = require('../../models/sta/Talent/Talent');
const Upbringing = require('../../models/sta/Upbringing/Upbringing');
const Environment = require('../../models/sta/Environment/Environment');

const { assert } = chai;
const { ObjectId } = mongoose.Types;

let testCharacterId;
const testCharacter = new Character({
  name: 'Eomer Éadig',
  age: 35,
  gender: 'male',
  species: 'human',
  rank: 'King of Rohan',
  environment: {
    selectedEnvironment: '5dbf41e5c64390867e1a9687',
    selectedStats: [
      {
        stat: 'reason',
        type: 'attribute',
        modifier: '1',
      },
      {
        stat: 'command',
        type: 'discipline',
        modifier: '1',
      },
    ],
  },
  upbringing: {
    selectedUpbringing: '5dbf4248deeba4cbe5e53207',
    status: 'accept',
    selectedStats: [
      {
        stat: 'reason',
        type: 'attribute',
        modifier: '1',
      },
      {
        stat: 'command',
        type: 'discipline',
        modifier: '1',
      },
    ],
  },
  attributes: {
    control: 7,
    daring: 8,
    fitness: 9,
    insight: 10,
    presence: 11,
    reason: 12,
  },
  disciplines: {
    command: 12,
    conn: 11,
    security: 10,
    engineering: 9,
    science: 8,
    medicine: 7,
  },
  values: [
    'One does not simply walk into mordor',
    'They are taking the hobbits to Isengard',
    'That still only counts as one',
  ],
  focuses: ['Horse riding', 'Spear throwing', 'Epic quotes'],
  talents: [
    '5dbf4248deeba4cbe5e53207',
    '5dbf4390414d42c02510488f',
    '5dbf43944075ac57d3a9b5f1',
  ],
  stress: {
    current: 0,
    maximum: 15,
  },
  determination: {
    current: 2,
    maximum: 3,
  },
  serviceRecord: [],
  equipment: [],
  bio: 'An awesome person, great horselord',
  milestones: {
    regular: 3,
    spotlight: 2,
    arc: 1,
  },
  reputation: 18,
  commendations: [],
});

chai.use(chaiHttp);

describe('Basic CRUD operations - Character', () => {
  before((done) => {
    mongoose.connection.collections.characters.drop(() => {
      Talent.create([
        {
          _id: ObjectId('5dbf4248deeba4cbe5e53207'),
          name: 'talent 1',
          description: 'description 1',
          category: 'general',
          requirements: [
            {
              stat: 'reason',
              type: 'attribute',
              modifier: 1,
            },
          ],
        },
        {
          _id: ObjectId('5dbf4390414d42c02510488f'),
          name: 'talent 2',
          description: 'description 2',
          category: 'general',
          requirements: [
            {
              stat: 'reason',
              type: 'attribute',
              modifier: 1,
            },
          ],
        },
        {
          _id: ObjectId('5dbf43944075ac57d3a9b5f1'),
          name: 'talent 3',
          description: 'description 3',
          category: 'general',
          requirements: [
            {
              stat: 'reason',
              type: 'attribute',
              modifier: 1,
            },
          ],
        },
      ]);

      Environment.create({
        _id: ObjectId('5dbf41e5c64390867e1a9687'),
        name: 'environment',
        description: 'description of an environment',
        attributes: [
          {
            stat: 'presence',
            type: 'attribute',
            modifier: 2,
          },
        ],
        disciplines: [
          {
            stat: 'science',
            type: 'discipline',
            modifier: 1,
          },
        ],
      });

      Upbringing.create({
        _id: ObjectId('5dbf4248deeba4cbe5e53207'),
        name: 'a new upbringing',
        description: 'description of an upbringing',
        accept: [
          {
            stat: 'presence',
            type: 'attribute',
            modifier: 2,
          },
        ],
        rebel: [
          {
            stat: 'fitness',
            type: 'attribute',
            modifier: 2,
          },
        ],
        disciplines: [
          {
            stat: 'science',
            type: 'discipline',
            modifier: 1,
          },
        ],
      });

      done();
    });
  });

  after((done) => {
    mongoose.connection.collections.talents.drop(() => {
      mongoose.connection.collections.upbringings.drop(() => {
        mongoose.connection.collections.environments.drop(() => {
          done();
        });
      });
    });
  });

  it('creates a character', (done) => {
    chai
      .request(app)
      .post('/sta/character/create')
      .send(testCharacter)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        testCharacterId = res.body.payload._id;

        const character = res.body.payload;

        assert.strictEqual(res.status, 201);
        assert.isObject(res.body);

        assert.strictEqual(character.name, 'Eomer Éadig');
        assert.strictEqual(character.age, 35);
        assert.strictEqual(character.gender, 'male');
        assert.strictEqual(character.species, 'human');
        assert.strictEqual(character.rank, 'King of Rohan');

        assert.isObject(character.environment);
        assert.isObject(character.environment.selectedEnvironment);
        assert.isString(character.environment.selectedEnvironment.name);
        assert.isString(character.environment.selectedEnvironment.description);
        assert.isArray(character.environment.selectedEnvironment.attributes);
        assert.isArray(character.environment.selectedEnvironment.disciplines);
        assert.isArray(character.environment.selectedStats);
        assert.strictEqual(character.environment.selectedStats.length, 2);
        assert.strictEqual(
          character.environment.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.environment.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.environment.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.environment.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.environment.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.environment.selectedStats[1].modifier, 1);

        assert.isObject(character.upbringing);
        assert.isObject(character.upbringing.selectedUpbringing);
        assert.isString(character.upbringing.selectedUpbringing.name);
        assert.isString(character.upbringing.selectedUpbringing.description);
        assert.isArray(character.upbringing.selectedUpbringing.accept);
        assert.isArray(character.upbringing.selectedUpbringing.rebel);
        assert.isArray(character.upbringing.selectedUpbringing.disciplines);
        assert.strictEqual(character.upbringing.status, 'accept');
        assert.isArray(character.upbringing.selectedStats);
        assert.strictEqual(character.upbringing.selectedStats.length, 2);
        assert.strictEqual(
          character.upbringing.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.upbringing.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.upbringing.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.upbringing.selectedStats[1].modifier, 1);

        assert.isObject(character.attributes);
        assert.strictEqual(character.attributes.control, 7);
        assert.strictEqual(character.attributes.daring, 8);
        assert.strictEqual(character.attributes.fitness, 9);
        assert.strictEqual(character.attributes.insight, 10);
        assert.strictEqual(character.attributes.presence, 11);
        assert.strictEqual(character.attributes.reason, 12);

        assert.isObject(character.disciplines);
        assert.strictEqual(character.disciplines.command, 12);
        assert.strictEqual(character.disciplines.conn, 11);
        assert.strictEqual(character.disciplines.security, 10);
        assert.strictEqual(character.disciplines.engineering, 9);
        assert.strictEqual(character.disciplines.science, 8);
        assert.strictEqual(character.disciplines.medicine, 7);

        assert.isArray(character.values);
        assert.strictEqual(character.values.length, 3);
        assert.strictEqual(
          character.values[0],
          'One does not simply walk into mordor',
        );
        assert.strictEqual(
          character.values[1],
          'They are taking the hobbits to Isengard',
        );
        assert.strictEqual(
          character.values[2],
          'That still only counts as one',
        );

        assert.isArray(character.focuses);
        assert.strictEqual(character.focuses.length, 3);
        assert.strictEqual(character.focuses[0], 'Horse riding');
        assert.strictEqual(character.focuses[1], 'Spear throwing');
        assert.strictEqual(character.focuses[2], 'Epic quotes');

        assert.isArray(character.talents);
        assert.strictEqual(character.talents.length, 3);
        assert.isObject(character.talents[0]);
        assert.isString(character.talents[0].name);
        assert.isString(character.talents[0].description);
        assert.isString(character.talents[0].category);
        assert.isArray(character.talents[0].requirements);
        assert.isObject(character.talents[1]);
        assert.isString(character.talents[1].name);
        assert.isString(character.talents[1].description);
        assert.isString(character.talents[1].category);
        assert.isArray(character.talents[1].requirements);
        assert.isObject(character.talents[2]);
        assert.isString(character.talents[2].name);
        assert.isString(character.talents[2].description);
        assert.isString(character.talents[2].category);
        assert.isArray(character.talents[2].requirements);

        assert.isObject(character.stress);
        assert.strictEqual(character.stress.current, 0);
        assert.strictEqual(character.stress.maximum, 15);

        assert.isObject(character.determination);
        assert.strictEqual(character.determination.current, 2);
        assert.strictEqual(character.determination.maximum, 3);

        assert.isArray(character.serviceRecord);

        assert.isArray(character.equipment);

        assert.strictEqual(character.bio, 'An awesome person, great horselord');

        assert.isObject(character.milestones);
        assert.strictEqual(character.milestones.regular, 3);
        assert.strictEqual(character.milestones.spotlight, 2);
        assert.strictEqual(character.milestones.arc, 1);

        assert.strictEqual(character.reputation, 18);

        assert.isArray(character.commendations);

        done();
      });
  });

  it('gets all characters', (done) => {
    chai
      .request(app)
      .get('/sta/character/')
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        const character = res.body.payload[0];

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body);

        assert.strictEqual(character.name, 'Eomer Éadig');
        assert.strictEqual(character.age, 35);
        assert.strictEqual(character.gender, 'male');
        assert.strictEqual(character.species, 'human');
        assert.strictEqual(character.rank, 'King of Rohan');

        assert.isObject(character.environment);
        assert.isObject(character.environment.selectedEnvironment);
        assert.isString(character.environment.selectedEnvironment.name);
        assert.isString(character.environment.selectedEnvironment.description);
        assert.isArray(character.environment.selectedEnvironment.attributes);
        assert.isArray(character.environment.selectedEnvironment.disciplines);
        assert.isArray(character.environment.selectedStats);
        assert.strictEqual(character.environment.selectedStats.length, 2);
        assert.strictEqual(
          character.environment.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.environment.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.environment.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.environment.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.environment.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.environment.selectedStats[1].modifier, 1);

        assert.isObject(character.upbringing);
        assert.isObject(character.upbringing.selectedUpbringing);
        assert.isString(character.upbringing.selectedUpbringing.name);
        assert.isString(character.upbringing.selectedUpbringing.description);
        assert.isArray(character.upbringing.selectedUpbringing.accept);
        assert.isArray(character.upbringing.selectedUpbringing.rebel);
        assert.isArray(character.upbringing.selectedUpbringing.disciplines);
        assert.strictEqual(character.upbringing.status, 'accept');
        assert.isArray(character.upbringing.selectedStats);
        assert.strictEqual(character.upbringing.selectedStats.length, 2);
        assert.strictEqual(
          character.upbringing.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.upbringing.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.upbringing.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.upbringing.selectedStats[1].modifier, 1);

        assert.isObject(character.attributes);
        assert.strictEqual(character.attributes.control, 7);
        assert.strictEqual(character.attributes.daring, 8);
        assert.strictEqual(character.attributes.fitness, 9);
        assert.strictEqual(character.attributes.insight, 10);
        assert.strictEqual(character.attributes.presence, 11);
        assert.strictEqual(character.attributes.reason, 12);

        assert.isObject(character.disciplines);
        assert.strictEqual(character.disciplines.command, 12);
        assert.strictEqual(character.disciplines.conn, 11);
        assert.strictEqual(character.disciplines.security, 10);
        assert.strictEqual(character.disciplines.engineering, 9);
        assert.strictEqual(character.disciplines.science, 8);
        assert.strictEqual(character.disciplines.medicine, 7);

        assert.isArray(character.values);
        assert.strictEqual(character.values.length, 3);
        assert.strictEqual(
          character.values[0],
          'One does not simply walk into mordor',
        );
        assert.strictEqual(
          character.values[1],
          'They are taking the hobbits to Isengard',
        );
        assert.strictEqual(
          character.values[2],
          'That still only counts as one',
        );

        assert.isArray(character.focuses);
        assert.strictEqual(character.focuses.length, 3);
        assert.strictEqual(character.focuses[0], 'Horse riding');
        assert.strictEqual(character.focuses[1], 'Spear throwing');
        assert.strictEqual(character.focuses[2], 'Epic quotes');

        assert.isArray(character.talents);
        assert.strictEqual(character.talents.length, 3);
        assert.isObject(character.talents[0]);
        assert.isString(character.talents[0].name);
        assert.isString(character.talents[0].description);
        assert.isString(character.talents[0].category);
        assert.isArray(character.talents[0].requirements);
        assert.isObject(character.talents[1]);
        assert.isString(character.talents[1].name);
        assert.isString(character.talents[1].description);
        assert.isString(character.talents[1].category);
        assert.isArray(character.talents[1].requirements);
        assert.isObject(character.talents[2]);
        assert.isString(character.talents[2].name);
        assert.isString(character.talents[2].description);
        assert.isString(character.talents[2].category);
        assert.isArray(character.talents[2].requirements);

        assert.isObject(character.stress);
        assert.strictEqual(character.stress.current, 0);
        assert.strictEqual(character.stress.maximum, 15);

        assert.isObject(character.determination);
        assert.strictEqual(character.determination.current, 2);
        assert.strictEqual(character.determination.maximum, 3);

        assert.isArray(character.serviceRecord);

        assert.isArray(character.equipment);

        assert.strictEqual(character.bio, 'An awesome person, great horselord');

        assert.isObject(character.milestones);
        assert.strictEqual(character.milestones.regular, 3);
        assert.strictEqual(character.milestones.spotlight, 2);
        assert.strictEqual(character.milestones.arc, 1);

        assert.strictEqual(character.reputation, 18);

        assert.isArray(character.commendations);

        done();
      });
  });

  it('gets character by id', (done) => {
    chai
      .request(app)
      .get(`/sta/character/${testCharacterId}`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        const character = res.body.payload;

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body);

        assert.strictEqual(character.name, 'Eomer Éadig');
        assert.strictEqual(character.age, 35);
        assert.strictEqual(character.gender, 'male');
        assert.strictEqual(character.species, 'human');
        assert.strictEqual(character.rank, 'King of Rohan');

        assert.isObject(character.environment);
        assert.isObject(character.environment.selectedEnvironment);
        assert.isString(character.environment.selectedEnvironment.name);
        assert.isString(character.environment.selectedEnvironment.description);
        assert.isArray(character.environment.selectedEnvironment.attributes);
        assert.isArray(character.environment.selectedEnvironment.disciplines);
        assert.isArray(character.environment.selectedStats);
        assert.strictEqual(character.environment.selectedStats.length, 2);
        assert.strictEqual(
          character.environment.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.environment.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.environment.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.environment.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.environment.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.environment.selectedStats[1].modifier, 1);

        assert.isObject(character.upbringing);
        assert.isObject(character.upbringing.selectedUpbringing);
        assert.isString(character.upbringing.selectedUpbringing.name);
        assert.isString(character.upbringing.selectedUpbringing.description);
        assert.isArray(character.upbringing.selectedUpbringing.accept);
        assert.isArray(character.upbringing.selectedUpbringing.rebel);
        assert.isArray(character.upbringing.selectedUpbringing.disciplines);
        assert.strictEqual(character.upbringing.status, 'accept');
        assert.isArray(character.upbringing.selectedStats);
        assert.strictEqual(character.upbringing.selectedStats.length, 2);
        assert.strictEqual(
          character.upbringing.selectedStats[0].stat,
          'reason',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.upbringing.selectedStats[0].modifier, 1);
        assert.strictEqual(
          character.upbringing.selectedStats[1].stat,
          'command',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.upbringing.selectedStats[1].modifier, 1);

        assert.isObject(character.attributes);
        assert.strictEqual(character.attributes.control, 7);
        assert.strictEqual(character.attributes.daring, 8);
        assert.strictEqual(character.attributes.fitness, 9);
        assert.strictEqual(character.attributes.insight, 10);
        assert.strictEqual(character.attributes.presence, 11);
        assert.strictEqual(character.attributes.reason, 12);

        assert.isObject(character.disciplines);
        assert.strictEqual(character.disciplines.command, 12);
        assert.strictEqual(character.disciplines.conn, 11);
        assert.strictEqual(character.disciplines.security, 10);
        assert.strictEqual(character.disciplines.engineering, 9);
        assert.strictEqual(character.disciplines.science, 8);
        assert.strictEqual(character.disciplines.medicine, 7);

        assert.isArray(character.values);
        assert.strictEqual(character.values.length, 3);
        assert.strictEqual(
          character.values[0],
          'One does not simply walk into mordor',
        );
        assert.strictEqual(
          character.values[1],
          'They are taking the hobbits to Isengard',
        );
        assert.strictEqual(
          character.values[2],
          'That still only counts as one',
        );

        assert.isArray(character.focuses);
        assert.strictEqual(character.focuses.length, 3);
        assert.strictEqual(character.focuses[0], 'Horse riding');
        assert.strictEqual(character.focuses[1], 'Spear throwing');
        assert.strictEqual(character.focuses[2], 'Epic quotes');

        assert.isArray(character.talents);
        assert.strictEqual(character.talents.length, 3);
        assert.isObject(character.talents[0]);
        assert.isString(character.talents[0].name);
        assert.isString(character.talents[0].description);
        assert.isString(character.talents[0].category);
        assert.isArray(character.talents[0].requirements);
        assert.isObject(character.talents[1]);
        assert.isString(character.talents[1].name);
        assert.isString(character.talents[1].description);
        assert.isString(character.talents[1].category);
        assert.isArray(character.talents[1].requirements);
        assert.isObject(character.talents[2]);
        assert.isString(character.talents[2].name);
        assert.isString(character.talents[2].description);
        assert.isString(character.talents[2].category);
        assert.isArray(character.talents[2].requirements);

        assert.isObject(character.stress);
        assert.strictEqual(character.stress.current, 0);
        assert.strictEqual(character.stress.maximum, 15);

        assert.isObject(character.determination);
        assert.strictEqual(character.determination.current, 2);
        assert.strictEqual(character.determination.maximum, 3);

        assert.isArray(character.serviceRecord);

        assert.isArray(character.equipment);

        assert.strictEqual(character.bio, 'An awesome person, great horselord');

        assert.isObject(character.milestones);
        assert.strictEqual(character.milestones.regular, 3);
        assert.strictEqual(character.milestones.spotlight, 2);
        assert.strictEqual(character.milestones.arc, 1);

        assert.strictEqual(character.reputation, 18);

        assert.isArray(character.commendations);

        done();
      });
  });

  it('updates a character', (done) => {
    chai
      .request(app)
      .put(`/sta/character/${testCharacterId}/edit`)
      .send({
        name: 'Frodo Baggins',
        age: 50,
        gender: 'male',
        species: 'human',
        rank: 'Ringbearer',
        environment: {
          selectedEnvironment: '5dbf41e5c64390867e1a9687',
          selectedStats: [
            {
              stat: 'presence',
              type: 'attribute',
              modifier: '2',
            },
            {
              stat: 'science',
              type: 'discipline',
              modifier: '2',
            },
          ],
        },
        upbringing: {
          selectedUpbringing: '5dbf4248deeba4cbe5e53207',
          status: 'rebel',
          selectedStats: [
            {
              stat: 'fitness',
              type: 'attribute',
              modifier: '2',
            },
            {
              stat: 'insight',
              type: 'discipline',
              modifier: '2',
            },
          ],
        },
        attributes: {
          control: 12,
          daring: 11,
          fitness: 10,
          insight: 9,
          presence: 8,
          reason: 7,
        },
        disciplines: {
          command: 7,
          conn: 8,
          security: 9,
          engineering: 10,
          science: 11,
          medicine: 12,
        },
        values: [
          'One simply walks into mordor',
          'But gets stung by a huge spider on the way',
          'Sam saves the day, always',
        ],
        focuses: ['Walking', 'More Walking', 'Even More Walking'],
        talents: [
          '5dbf43944075ac57d3a9b5f1',
          '5dbf4390414d42c02510488f',
          '5dbf4248deeba4cbe5e53207',
        ],
        stress: {
          current: 5,
          maximum: 15,
        },
        determination: {
          current: 0,
          maximum: 3,
        },
        serviceRecord: [],
        equipment: [],
        bio: 'Bears the ring to Mordor',
        milestones: {
          regular: 2,
          spotlight: 1,
          arc: 0,
        },
        reputation: 20,
        commendations: [],
      })
      .end((err, res) => {
        if (err) {
          done();
          return;
        }

        testCharacterId = res.body.payload._id;

        const character = res.body.payload;

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body);

        assert.strictEqual(character.name, 'Frodo Baggins');
        assert.strictEqual(character.age, 50);
        assert.strictEqual(character.gender, 'male');
        assert.strictEqual(character.species, 'human');
        assert.strictEqual(character.rank, 'Ringbearer');

        assert.isObject(character.environment);
        assert.isObject(character.environment.selectedEnvironment);
        assert.isString(character.environment.selectedEnvironment.name);
        assert.isString(character.environment.selectedEnvironment.description);
        assert.isArray(character.environment.selectedEnvironment.attributes);
        assert.isArray(character.environment.selectedEnvironment.disciplines);
        assert.isArray(character.environment.selectedStats);
        assert.strictEqual(character.environment.selectedStats.length, 2);
        assert.strictEqual(
          character.environment.selectedStats[0].stat,
          'presence',
        );
        assert.strictEqual(
          character.environment.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.environment.selectedStats[0].modifier, 2);
        assert.strictEqual(
          character.environment.selectedStats[1].stat,
          'science',
        );
        assert.strictEqual(
          character.environment.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.environment.selectedStats[1].modifier, 2);

        assert.isObject(character.upbringing);
        assert.isObject(character.upbringing.selectedUpbringing);
        assert.isString(character.upbringing.selectedUpbringing.name);
        assert.isString(character.upbringing.selectedUpbringing.description);
        assert.isArray(character.upbringing.selectedUpbringing.accept);
        assert.isArray(character.upbringing.selectedUpbringing.rebel);
        assert.isArray(character.upbringing.selectedUpbringing.disciplines);
        assert.strictEqual(character.upbringing.status, 'rebel');
        assert.isArray(character.upbringing.selectedStats);
        assert.strictEqual(character.upbringing.selectedStats.length, 2);
        assert.strictEqual(
          character.upbringing.selectedStats[0].stat,
          'fitness',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[0].type,
          'attribute',
        );
        assert.strictEqual(character.upbringing.selectedStats[0].modifier, 2);
        assert.strictEqual(
          character.upbringing.selectedStats[1].stat,
          'insight',
        );
        assert.strictEqual(
          character.upbringing.selectedStats[1].type,
          'discipline',
        );
        assert.strictEqual(character.upbringing.selectedStats[1].modifier, 2);

        assert.isObject(character.attributes);
        assert.strictEqual(character.attributes.control, 12);
        assert.strictEqual(character.attributes.daring, 11);
        assert.strictEqual(character.attributes.fitness, 10);
        assert.strictEqual(character.attributes.insight, 9);
        assert.strictEqual(character.attributes.presence, 8);
        assert.strictEqual(character.attributes.reason, 7);

        assert.isObject(character.disciplines);
        assert.strictEqual(character.disciplines.command, 7);
        assert.strictEqual(character.disciplines.conn, 8);
        assert.strictEqual(character.disciplines.security, 9);
        assert.strictEqual(character.disciplines.engineering, 10);
        assert.strictEqual(character.disciplines.science, 11);
        assert.strictEqual(character.disciplines.medicine, 12);

        assert.isArray(character.values);
        assert.strictEqual(character.values.length, 3);
        assert.strictEqual(character.values[0], 'One simply walks into mordor');
        assert.strictEqual(
          character.values[1],
          'But gets stung by a huge spider on the way',
        );
        assert.strictEqual(character.values[2], 'Sam saves the day, always');

        assert.isArray(character.focuses);
        assert.strictEqual(character.focuses.length, 3);
        assert.strictEqual(character.focuses[0], 'Walking');
        assert.strictEqual(character.focuses[1], 'More Walking');
        assert.strictEqual(character.focuses[2], 'Even More Walking');

        assert.isArray(character.talents);
        assert.strictEqual(character.talents.length, 3);
        assert.isObject(character.talents[0]);
        assert.isString(character.talents[0].name);
        assert.isString(character.talents[0].description);
        assert.isString(character.talents[0].category);
        assert.isArray(character.talents[0].requirements);
        assert.isObject(character.talents[1]);
        assert.isString(character.talents[1].name);
        assert.isString(character.talents[1].description);
        assert.isString(character.talents[1].category);
        assert.isArray(character.talents[1].requirements);
        assert.isObject(character.talents[2]);
        assert.isString(character.talents[2].name);
        assert.isString(character.talents[2].description);
        assert.isString(character.talents[2].category);
        assert.isArray(character.talents[2].requirements);

        assert.isObject(character.stress);
        assert.strictEqual(character.stress.current, 5);
        assert.strictEqual(character.stress.maximum, 15);

        assert.isObject(character.determination);
        assert.strictEqual(character.determination.current, 0);
        assert.strictEqual(character.determination.maximum, 3);

        assert.isArray(character.serviceRecord);

        assert.isArray(character.equipment);

        assert.strictEqual(character.bio, 'Bears the ring to Mordor');

        assert.isObject(character.milestones);
        assert.strictEqual(character.milestones.regular, 2);
        assert.strictEqual(character.milestones.spotlight, 1);
        assert.strictEqual(character.milestones.arc, 0);

        assert.strictEqual(character.reputation, 20);

        assert.isArray(character.commendations);

        done();
      });
  });

  it('deletes a character', (done) => {
    chai
      .request(app)
      .delete(`/sta/character/${testCharacterId}/delete`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        done();
      });
  });
});
