const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const Environment = require('../../models/sta/Environment/Environment');

const { assert } = chai;

let testEnvironmentId;
const testEnvironment = new Environment({
  name: 'a new environment',
  description: 'description of an environment',
  attributes: [
    {
      stat: 'presence',
      type: 'attribute',
      modifier: 2,
    },
    {
      stat: 'insight',
      type: 'attribute',
      modifier: 1,
    },
  ],
  disciplines: [
    {
      stat: 'science',
      type: 'discipline',
      modifier: 1,
    },
    {
      stat: 'medicine',
      type: 'discipline',
      modifier: 2,
    },
  ],
});

chai.use(chaiHttp);

describe('Basic CRUD operations - Environment', () => {
  before((done) => {
    mongoose.connection.collections.environments.drop(() => {
      done();
    });
  });

  it('creates a environment', (done) => {
    chai
      .request(app)
      .post('/sta/environment/create')
      .send(testEnvironment)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        testEnvironmentId = res.body.payload._id;

        assert.strictEqual(res.status, 201);
        assert.isObject(res.body);

        assert.strictEqual(res.body.payload.name, 'a new environment');
        assert.strictEqual(
          res.body.payload.description,
          'description of an environment',
        );

        assert.isArray(res.body.payload.attributes);
        assert.strictEqual(res.body.payload.attributes.length, 2);
        assert.strictEqual(res.body.payload.attributes[0].stat, 'presence');
        assert.strictEqual(res.body.payload.attributes[0].type, 'attribute');
        assert.strictEqual(res.body.payload.attributes[0].modifier, 2);
        assert.strictEqual(res.body.payload.attributes[1].stat, 'insight');
        assert.strictEqual(res.body.payload.attributes[1].type, 'attribute');
        assert.strictEqual(res.body.payload.attributes[1].modifier, 1);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 2);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'science');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload.disciplines[1].stat, 'medicine');
        assert.strictEqual(res.body.payload.disciplines[1].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[1].modifier, 2);
        done();
      });
  });

  it('gets all environments', (done) => {
    chai
      .request(app)
      .get('/sta/environment/')
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.strictEqual(res.body.payload.length, 1);
        assert.isDefined(res.body.payload[0]);
        assert.strictEqual(res.body.payload[0]._id, testEnvironmentId);
        assert.strictEqual(res.body.payload[0].name, 'a new environment');
        assert.strictEqual(
          res.body.payload[0].description,
          'description of an environment',
        );

        assert.isArray(res.body.payload[0].attributes);
        assert.strictEqual(res.body.payload[0].attributes.length, 2);
        assert.strictEqual(res.body.payload[0].attributes[0].stat, 'presence');
        assert.strictEqual(res.body.payload[0].attributes[0].type, 'attribute');
        assert.strictEqual(res.body.payload[0].attributes[0].modifier, 2);
        assert.strictEqual(res.body.payload[0].attributes[1].stat, 'insight');
        assert.strictEqual(res.body.payload[0].attributes[1].type, 'attribute');
        assert.strictEqual(res.body.payload[0].attributes[1].modifier, 1);

        assert.isArray(res.body.payload[0].disciplines);
        assert.strictEqual(res.body.payload[0].disciplines.length, 2);
        assert.strictEqual(res.body.payload[0].disciplines[0].stat, 'science');
        assert.strictEqual(
          res.body.payload[0].disciplines[0].type,
          'discipline',
        );
        assert.strictEqual(res.body.payload[0].disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload[0].disciplines[1].stat, 'medicine');
        assert.strictEqual(
          res.body.payload[0].disciplines[1].type,
          'discipline',
        );
        assert.strictEqual(res.body.payload[0].disciplines[1].modifier, 2);
        done();
      });
  });

  it('gets environment by id', (done) => {
    chai
      .request(app)
      .get(`/sta/environment/${testEnvironmentId}`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isDefined(res.body.payload);
        assert.strictEqual(res.body.payload._id, testEnvironmentId);
        assert.strictEqual(res.body.payload.name, 'a new environment');
        assert.strictEqual(
          res.body.payload.description,
          'description of an environment',
        );

        assert.isArray(res.body.payload.attributes);
        assert.strictEqual(res.body.payload.attributes.length, 2);
        assert.strictEqual(res.body.payload.attributes[0].stat, 'presence');
        assert.strictEqual(res.body.payload.attributes[0].type, 'attribute');
        assert.strictEqual(res.body.payload.attributes[0].modifier, 2);
        assert.strictEqual(res.body.payload.attributes[1].stat, 'insight');
        assert.strictEqual(res.body.payload.attributes[1].type, 'attribute');
        assert.strictEqual(res.body.payload.attributes[1].modifier, 1);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 2);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'science');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload.disciplines[1].stat, 'medicine');
        assert.strictEqual(res.body.payload.disciplines[1].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[1].modifier, 2);
        done();
      });
  });

  it('updates a environment', (done) => {
    chai
      .request(app)
      .put(`/sta/environment/${testEnvironmentId}/edit`)
      .send({
        name: 'an updated environment',
        description: 'updated description',
        attributes: [
          {
            stat: 'fitness',
            type: 'attribute',
            modifier: 1,
          },
        ],
        disciplines: [
          {
            stat: 'medicine',
            type: 'discipline',
            modifier: 2,
          },
        ],
      })
      .end((err, res) => {
        if (err) {
          done();
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body.payload);
        assert.strictEqual(res.body.payload.name, 'an updated environment');
        assert.strictEqual(res.body.payload.description, 'updated description');

        assert.isArray(res.body.payload.attributes);
        assert.strictEqual(res.body.payload.attributes.length, 1);
        assert.strictEqual(res.body.payload.attributes[0].stat, 'fitness');
        assert.strictEqual(res.body.payload.attributes[0].type, 'attribute');
        assert.strictEqual(res.body.payload.attributes[0].modifier, 1);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 1);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'medicine');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 2);
        done();
      });
  });

  it('deletes a environment', (done) => {
    chai
      .request(app)
      .delete(`/sta/environment/${testEnvironmentId}/delete`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        done();
      });
  });
});
