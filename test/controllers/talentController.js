const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const Talent = require('../../models/sta/Talent/Talent');

const { assert } = chai;

let testTalentId;
const testTalent = new Talent({
  name: 'a new talent',
  description: 'description of a talent',
  category: 'general',
  requirements: [
    {
      stat: 'reason',
      type: 'attribute',
      modifier: 5,
    },
  ],
});

chai.use(chaiHttp);

describe('Basic CRUD operations - Talent', () => {
  before((done) => {
    mongoose.connection.collections.talents.drop(() => {
      done();
    });
  });

  it('creates a talent', (done) => {
    chai
      .request(app)
      .post('/sta/talent/create')
      .send(testTalent)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        testTalentId = res.body.payload._id;

        assert.strictEqual(res.status, 201);
        assert.isObject(res.body);
        assert.strictEqual(res.body.payload.name, 'a new talent');
        assert.strictEqual(
          res.body.payload.description,
          'description of a talent',
        );
        assert.strictEqual(res.body.payload.category, 'general');
        assert.isDefined(res.body.payload.requirements[0]);
        assert.strictEqual(res.body.payload.requirements[0].stat, 'reason');
        assert.strictEqual(res.body.payload.requirements[0].type, 'attribute');
        assert.strictEqual(res.body.payload.requirements[0].modifier, 5);
        done();
      });
  });

  it('gets all talents', (done) => {
    chai
      .request(app)
      .get('/sta/talent/')
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.strictEqual(res.body.payload.length, 1);
        assert.isDefined(res.body.payload[0]);
        assert.strictEqual(res.body.payload[0]._id, testTalentId);
        assert.strictEqual(res.body.payload[0].name, 'a new talent');
        assert.strictEqual(
          res.body.payload[0].description,
          'description of a talent',
        );
        assert.strictEqual(res.body.payload[0].category, 'general');
        assert.isDefined(res.body.payload[0].requirements[0]);
        assert.strictEqual(res.body.payload[0].requirements[0].stat, 'reason');
        assert.strictEqual(
          res.body.payload[0].requirements[0].type,
          'attribute',
        );
        assert.strictEqual(res.body.payload[0].requirements[0].modifier, 5);
        done();
      });
  });

  it('gets talent by id', (done) => {
    chai
      .request(app)
      .get(`/sta/talent/${testTalentId}`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isDefined(res.body.payload);
        assert.strictEqual(res.body.payload._id, testTalentId);
        assert.strictEqual(res.body.payload.name, 'a new talent');
        assert.strictEqual(
          res.body.payload.description,
          'description of a talent',
        );
        assert.strictEqual(res.body.payload.category, 'general');
        assert.isDefined(res.body.payload.requirements[0]);
        assert.strictEqual(res.body.payload.requirements[0].stat, 'reason');
        assert.strictEqual(res.body.payload.requirements[0].type, 'attribute');
        assert.strictEqual(res.body.payload.requirements[0].modifier, 5);
        done();
      });
  });

  it('updates a talent', (done) => {
    chai
      .request(app)
      .put(`/sta/talent/${testTalentId}/edit`)
      .send({
        name: 'updated talent',
        description: 'updated description',
        category: 'command',
        requirements: [
          {
            stat: 'command',
            type: 'discipline',
            modifier: 4,
          },
        ],
      })
      .end((err, res) => {
        if (err) {
          done();
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body.payload);
        assert.strictEqual(res.body.payload.name, 'updated talent');
        assert.strictEqual(res.body.payload.description, 'updated description');
        assert.strictEqual(res.body.payload.category, 'command');
        assert.isDefined(res.body.payload.requirements[0]);
        assert.strictEqual(res.body.payload.requirements[0].stat, 'command');
        assert.strictEqual(res.body.payload.requirements[0].type, 'discipline');
        assert.strictEqual(res.body.payload.requirements[0].modifier, 4);
        done();
      });
  });

  it('deletes a talent', (done) => {
    chai
      .request(app)
      .delete(`/sta/talent/${testTalentId}/delete`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        done();
      });
  });
});
