const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const Upbringing = require('../../models/sta/Upbringing/Upbringing');

const { assert } = chai;

let testUpbringingId;
const testUpbringing = new Upbringing({
  name: 'a new upbringing',
  description: 'description of an upbringing',
  accept: [
    {
      stat: 'presence',
      type: 'attribute',
      modifier: 2,
    },
    {
      stat: 'insight',
      type: 'attribute',
      modifier: 1,
    },
  ],
  rebel: [
    {
      stat: 'fitness',
      type: 'attribute',
      modifier: 2,
    },
    {
      stat: 'control',
      type: 'attribute',
      modifier: 1,
    },
  ],
  disciplines: [
    {
      stat: 'science',
      type: 'discipline',
      modifier: 1,
    },
    {
      stat: 'medicine',
      type: 'discipline',
      modifier: 2,
    },
  ],
});

chai.use(chaiHttp);

describe('Basic CRUD operations - Upbringing', () => {
  before((done) => {
    mongoose.connection.collections.upbringings.drop(() => {
      done();
    });
  });

  it('creates a upbringing', (done) => {
    chai
      .request(app)
      .post('/sta/upbringing/create')
      .send(testUpbringing)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        testUpbringingId = res.body.payload._id;

        assert.strictEqual(res.status, 201);
        assert.isObject(res.body);

        assert.strictEqual(res.body.payload.name, 'a new upbringing');
        assert.strictEqual(
          res.body.payload.description,
          'description of an upbringing',
        );

        assert.isArray(res.body.payload.accept);
        assert.strictEqual(res.body.payload.accept.length, 2);
        assert.strictEqual(res.body.payload.accept[0].stat, 'presence');
        assert.strictEqual(res.body.payload.accept[0].type, 'attribute');
        assert.strictEqual(res.body.payload.accept[0].modifier, 2);
        assert.strictEqual(res.body.payload.accept[1].stat, 'insight');
        assert.strictEqual(res.body.payload.accept[1].type, 'attribute');
        assert.strictEqual(res.body.payload.accept[1].modifier, 1);

        assert.isArray(res.body.payload.rebel);
        assert.strictEqual(res.body.payload.rebel.length, 2);
        assert.strictEqual(res.body.payload.rebel[0].stat, 'fitness');
        assert.strictEqual(res.body.payload.rebel[0].type, 'attribute');
        assert.strictEqual(res.body.payload.rebel[0].modifier, 2);
        assert.strictEqual(res.body.payload.rebel[1].stat, 'control');
        assert.strictEqual(res.body.payload.rebel[1].type, 'attribute');
        assert.strictEqual(res.body.payload.rebel[1].modifier, 1);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 2);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'science');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload.disciplines[1].stat, 'medicine');
        assert.strictEqual(res.body.payload.disciplines[1].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[1].modifier, 2);
        done();
      });
  });

  it('gets all upbringings', (done) => {
    chai
      .request(app)
      .get('/sta/upbringing/')
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.strictEqual(res.body.payload.length, 1);
        assert.isDefined(res.body.payload[0]);
        assert.strictEqual(res.body.payload[0]._id, testUpbringingId);
        assert.strictEqual(res.body.payload[0].name, 'a new upbringing');
        assert.strictEqual(
          res.body.payload[0].description,
          'description of an upbringing',
        );

        assert.isArray(res.body.payload[0].accept);
        assert.strictEqual(res.body.payload[0].accept.length, 2);
        assert.strictEqual(res.body.payload[0].accept[0].stat, 'presence');
        assert.strictEqual(res.body.payload[0].accept[0].type, 'attribute');
        assert.strictEqual(res.body.payload[0].accept[0].modifier, 2);
        assert.strictEqual(res.body.payload[0].accept[1].stat, 'insight');
        assert.strictEqual(res.body.payload[0].accept[1].type, 'attribute');
        assert.strictEqual(res.body.payload[0].accept[1].modifier, 1);

        assert.isArray(res.body.payload[0].rebel);
        assert.strictEqual(res.body.payload[0].rebel.length, 2);
        assert.strictEqual(res.body.payload[0].rebel[0].stat, 'fitness');
        assert.strictEqual(res.body.payload[0].rebel[0].type, 'attribute');
        assert.strictEqual(res.body.payload[0].rebel[0].modifier, 2);
        assert.strictEqual(res.body.payload[0].rebel[1].stat, 'control');
        assert.strictEqual(res.body.payload[0].rebel[1].type, 'attribute');
        assert.strictEqual(res.body.payload[0].rebel[1].modifier, 1);

        assert.isArray(res.body.payload[0].disciplines);
        assert.strictEqual(res.body.payload[0].disciplines.length, 2);
        assert.strictEqual(res.body.payload[0].disciplines[0].stat, 'science');
        assert.strictEqual(
          res.body.payload[0].disciplines[0].type,
          'discipline',
        );
        assert.strictEqual(res.body.payload[0].disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload[0].disciplines[1].stat, 'medicine');
        assert.strictEqual(
          res.body.payload[0].disciplines[1].type,
          'discipline',
        );
        assert.strictEqual(res.body.payload[0].disciplines[1].modifier, 2);
        done();
      });
  });

  it('gets upbringing by id', (done) => {
    chai
      .request(app)
      .get(`/sta/upbringing/${testUpbringingId}`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isDefined(res.body.payload);
        assert.strictEqual(res.body.payload._id, testUpbringingId);
        assert.strictEqual(res.body.payload.name, 'a new upbringing');
        assert.strictEqual(
          res.body.payload.description,
          'description of an upbringing',
        );

        assert.isArray(res.body.payload.accept);
        assert.strictEqual(res.body.payload.accept.length, 2);
        assert.strictEqual(res.body.payload.accept[0].stat, 'presence');
        assert.strictEqual(res.body.payload.accept[0].type, 'attribute');
        assert.strictEqual(res.body.payload.accept[0].modifier, 2);
        assert.strictEqual(res.body.payload.accept[1].stat, 'insight');
        assert.strictEqual(res.body.payload.accept[1].type, 'attribute');
        assert.strictEqual(res.body.payload.accept[1].modifier, 1);

        assert.isArray(res.body.payload.rebel);
        assert.strictEqual(res.body.payload.rebel.length, 2);
        assert.strictEqual(res.body.payload.rebel[0].stat, 'fitness');
        assert.strictEqual(res.body.payload.rebel[0].type, 'attribute');
        assert.strictEqual(res.body.payload.rebel[0].modifier, 2);
        assert.strictEqual(res.body.payload.rebel[1].stat, 'control');
        assert.strictEqual(res.body.payload.rebel[1].type, 'attribute');
        assert.strictEqual(res.body.payload.rebel[1].modifier, 1);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 2);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'science');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 1);
        assert.strictEqual(res.body.payload.disciplines[1].stat, 'medicine');
        assert.strictEqual(res.body.payload.disciplines[1].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[1].modifier, 2);
        done();
      });
  });

  it('updates a upbringing', (done) => {
    chai
      .request(app)
      .put(`/sta/upbringing/${testUpbringingId}/edit`)
      .send({
        name: 'an updated upbringing',
        description: 'updated description',
        accept: [
          {
            stat: 'reason',
            type: 'attribute',
            modifier: 2,
          },
        ],
        rebel: [
          {
            stat: 'daring',
            type: 'attribute',
            modifier: 2,
          },
        ],
        disciplines: [
          {
            stat: 'conn',
            type: 'discipline',
            modifier: 2,
          },
        ],
      })
      .end((err, res) => {
        if (err) {
          done();
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body.payload);
        assert.strictEqual(res.body.payload.name, 'an updated upbringing');
        assert.strictEqual(res.body.payload.description, 'updated description');

        assert.isArray(res.body.payload.accept);
        assert.strictEqual(res.body.payload.accept.length, 1);
        assert.strictEqual(res.body.payload.accept[0].stat, 'reason');
        assert.strictEqual(res.body.payload.accept[0].type, 'attribute');
        assert.strictEqual(res.body.payload.accept[0].modifier, 2);

        assert.isArray(res.body.payload.rebel);
        assert.strictEqual(res.body.payload.rebel.length, 1);
        assert.strictEqual(res.body.payload.rebel[0].stat, 'daring');
        assert.strictEqual(res.body.payload.rebel[0].type, 'attribute');
        assert.strictEqual(res.body.payload.rebel[0].modifier, 2);

        assert.isArray(res.body.payload.disciplines);
        assert.strictEqual(res.body.payload.disciplines.length, 1);
        assert.strictEqual(res.body.payload.disciplines[0].stat, 'conn');
        assert.strictEqual(res.body.payload.disciplines[0].type, 'discipline');
        assert.strictEqual(res.body.payload.disciplines[0].modifier, 2);
        done();
      });
  });

  it('deletes a upbringing', (done) => {
    chai
      .request(app)
      .delete(`/sta/upbringing/${testUpbringingId}/delete`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        done();
      });
  });
});
