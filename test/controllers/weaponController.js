const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const Weapon = require('../../models/sta/Weapon/Weapon');

const { assert } = chai;

let testWeaponId;
const testWeapon = new Weapon({
  name: 'a new weapon',
  description: 'description of a weapon',
  type: 'ranged',
  size: 'two-handed',
  damageType: {
    numberOfDice: 5,
    effects: [],
  },
  qualities: [],
  cost: [],
});

chai.use(chaiHttp);

describe('Basic CRUD operations - Weapon', () => {
  before((done) => {
    mongoose.connection.collections.weapons.drop(() => {
      done();
    });
  });

  it('creates a weapon', (done) => {
    chai
      .request(app)
      .post('/sta/weapon/create')
      .send(testWeapon)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        testWeaponId = res.body.payload._id;

        assert.strictEqual(res.status, 201);
        assert.isObject(res.body);
        assert.isDefined(res.body.payload);

        const weapon = res.body.payload;

        assert.strictEqual(weapon.name, 'a new weapon');
        assert.strictEqual(weapon.description, 'description of a weapon');
        assert.strictEqual(weapon.type, 'ranged');
        assert.strictEqual(weapon.size, 'two-handed');
        assert.isDefined(weapon.damageType);
        assert.strictEqual(weapon.damageType.numberOfDice, 5);
        assert.isArray(weapon.damageType.effects);
        assert.isArray(weapon.qualities);
        assert.isArray(weapon.cost);
        done();
      });
  });

  it('gets all weapons', (done) => {
    chai
      .request(app)
      .get('/sta/weapon/')
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.strictEqual(res.body.payload.length, 1);
        assert.isDefined(res.body.payload[0]);

        const weapon = res.body.payload[0];

        assert.strictEqual(weapon.name, 'a new weapon');
        assert.strictEqual(weapon.description, 'description of a weapon');
        assert.strictEqual(weapon.type, 'ranged');
        assert.strictEqual(weapon.size, 'two-handed');
        assert.isDefined(weapon.damageType);
        assert.strictEqual(weapon.damageType.numberOfDice, 5);
        assert.isArray(weapon.damageType.effects);
        assert.isArray(weapon.qualities);
        assert.isArray(weapon.cost);
        done();
      });
  });

  it('gets weapon by id', (done) => {
    chai
      .request(app)
      .get(`/sta/weapon/${testWeaponId}`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isDefined(res.body.payload);

        const weapon = res.body.payload;

        assert.strictEqual(weapon.name, 'a new weapon');
        assert.strictEqual(weapon.description, 'description of a weapon');
        assert.strictEqual(weapon.type, 'ranged');
        assert.strictEqual(weapon.size, 'two-handed');
        assert.isDefined(weapon.damageType);
        assert.strictEqual(weapon.damageType.numberOfDice, 5);
        assert.isArray(weapon.damageType.effects);
        assert.isArray(weapon.qualities);
        assert.isArray(weapon.cost);
        done();
      });
  });

  it('updates a weapon', (done) => {
    chai
      .request(app)
      .put(`/sta/weapon/${testWeaponId}/edit`)
      .send({
        name: 'updated weapon',
        description: 'updated description',
        type: 'melee',
        size: 'one-handed',
        damageType: {
          numberOfDice: 2,
          effects: [],
        },
        qualities: [],
        cost: [],
      })
      .end((err, res) => {
        if (err) {
          done();
          return;
        }

        assert.strictEqual(res.status, 200);
        assert.isObject(res.body.payload);

        const weapon = res.body.payload;

        assert.strictEqual(weapon.name, 'updated weapon');
        assert.strictEqual(weapon.description, 'updated description');
        assert.strictEqual(weapon.type, 'melee');
        assert.strictEqual(weapon.size, 'one-handed');
        assert.isDefined(weapon.damageType);
        assert.strictEqual(weapon.damageType.numberOfDice, 2);
        assert.isArray(weapon.damageType.effects);
        assert.isArray(weapon.qualities);
        assert.isArray(weapon.cost);
        done();
      });
  });

  it('deletes a weapon', (done) => {
    chai
      .request(app)
      .delete(`/sta/weapon/${testWeaponId}/delete`)
      .end((err, res) => {
        if (err) {
          done(err);
          return;
        }

        assert.strictEqual(res.status, 200);
        done();
      });
  });
});
