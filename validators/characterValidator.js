const { checkSchema } = require('express-validator');

exports.validateCreateOrUpdateRequest = checkSchema({
  name: {
    in: ['body'],
    isString: true,
  },
  age: {
    in: ['body'],
    isNumeric: true,
  },
  gender: {
    in: ['body'],
    isString: true,
  },
  rank: {
    in: ['body'],
    isString: true,
  },
  species: {
    in: ['body'],
    isString: true,
  },
  bio: {
    in: ['body'],
    isString: true,
  },
  serviceRecord: {
    in: ['body'],
    isArray: true,
  },
  equipment: {
    in: ['body'],
    isArray: true,
  },
  values: {
    in: ['body'],
    isArray: true,
  },
  focuses: {
    in: ['body'],
    isArray: true,
  },
  talents: {
    in: ['body'],
    isArray: true,
  },
  attributes: {
    in: ['body'],
    isArray: true,
  },
  disciplines: {
    in: ['body'],
    isArray: true,
  },
  stress: {
    in: ['body'],
    exists: true,
  },
  determination: {
    in: ['body'],
    exists: true,
  },
});
