const { checkSchema } = require('express-validator');

exports.validateCreateOrUpdateRequest = checkSchema({
  name: {
    in: ['body'],
    isString: true,
  },
  description: {
    in: ['body'],
    isString: true,
  },
  attributes: {
    in: ['body'],
    exists: true,
  },
  disciplines: {
    in: ['body'],
    exists: true,
  },
});
