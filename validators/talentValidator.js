const { checkSchema } = require('express-validator');

exports.validateCreateOrUpdateRequest = checkSchema({
  name: {
    in: ['body'],
    isString: true,
  },
  description: {
    in: ['body'],
    isString: true,
  },
  category: {
    in: ['body'],
    isString: true,
  },
  requirement: {
    in: ['body'],
    isArray: true,
  },
});
