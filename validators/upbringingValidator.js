const { checkSchema } = require('express-validator');

exports.validateCreateOrUpdateRequest = checkSchema({
  name: {
    in: ['body'],
    isString: true,
  },
  description: {
    in: ['body'],
    isString: true,
  },
  accept: {
    in: ['body'],
    isArray: true,
  },
  rebel: {
    in: ['body'],
    isArray: true,
  },
  disciplines: {
    in: ['body'],
    isArray: true,
  },
});
