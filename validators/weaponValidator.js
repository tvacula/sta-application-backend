const { checkSchema } = require('express-validator');

exports.validateCreateOrUpdateRequest = checkSchema({
  name: {
    in: ['body'],
    isString: true,
  },
  description: {
    in: ['body'],
    isString: true,
  },
  type: {
    in: ['body'],
    isString: true,
  },
  size: {
    in: ['body'],
    isString: true,
  },
  damageType: {
    in: ['body'],
    exists: true,
  },
  qualities: {
    in: ['body'],
    isArray: true,
  },
  cost: {
    in: ['body'],
    isArray: true,
  },
});
